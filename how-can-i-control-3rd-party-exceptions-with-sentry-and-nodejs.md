# How can I control 3rd party exceptions with Sentry and NodeJS?

## Context

Date:  19-11-2019
Project: https://eaglestatus.io

The Slack channel where exceptions are being reported is not reliable. I have to distinguish between EXCEPTIONS and "exceptions".

The application is connecting to third party systems. A timeout or a 5XX error on their side raises an exception. The problem is that, in most cases, there is nothing I can do with that exception. It only adds noise.

On the other hand, I DO want to know when the errors repeat multiple times in a short period. Something may need attention.

The purpose of this spike is to figure out how to define alternative notification rules with Sentry and NodeJS.

## Research done

### Sending exceptions with warning level

Sentry allows to set the scope of an event. This allows attach a level to an exception.

```js
Sentry.withScope((scope) => {
    scope.setLevel('warning')
    Sentry.captureException(new Error('Third party error'))
  })
```

In my case, this can be used to report the exceptions of components that connect to other systems. The ones that can tolerate several errors.

This is a "traditional" exception:

```js
Sentry.captureException(new Error('Bad stuff'))
```

_Both kind of exceptions are visible in Sentry's dashboard._


### Configure Sentry to only report high rate of warnings to Slack

In my case, a high rate can be considered 10 warning instances in less than an hour.

Rules can be defined from the Sentry's dashboard: Organization > Settings > Integrations > Slack > Configure

This is how it looks after changing the rules (oh, that sounds cool xD).

![alt text](./media/sentry_slack_config.png "Sentry/Slack config")


### Testing the configuration

Configuration can be tested with this script:


```js
const Sentry = require('@sentry/node')
const dsn = 'xxxx'

Sentry.init({
  dsn,
  attachStackTrace: true
})

const TOTAL_EXCEPTIONS = 8
const ONE_SECOND = 1000

let i = 0
let interval

interval = setInterval(() => {
  if ( i === TOTAL_EXCEPTIONS) {
    clearInterval(interval)
    return
  }

  console.log('Sending warning...')
  Sentry.withScope((scope) => {
    scope.setLevel('warning')
    Sentry.captureException(new Error('Third party error'))
  })
  i += 1
}, ONE_SECOND)

console.log('Sending error...')
Sentry.captureException(new Error('Bad stuff'))
```

Notice:

- It reports multiple warnings
- It reports a single error


## Conclusions

- Sentry allows to assign a severity to an exception
- From its web interface rules can be defined. For example a minimum rate of warnings to send notifications.
- CONS: Configuration is done in the web interface. Not as code / config file.
- CONS: All warnings are under the same namespace, "warning". At this point of the project it can be assumed.
- CONS: The notification of a warning exception is the 10th of a sequence. Not necessary related to other exceptions. It requires someone to consult the dashboard and check the other warnings.


## Sources

- https://www.npmjs.com/package/@sentry/node
- https://sentry.io/integrations/slack/
- https://docs.sentry.io/workflow/integrations/global-integrations/#slack

## Related spikes

- [How can I save Cypress screenshots + logs in GitlabCI?](./how-can-i-save-cypress-screenshots-+-logs-in-gitlabci.md)