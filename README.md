# Spike notes

## Index

- [How can I load Google Analytics script only in production with Webpack?](./how-can-i-load-google-analytics-script-only-in-production-with-webpack.md)
- [How can I test GoogleAnalytics events with Cypress end-to-end testing?](how-can-i-test-google-analytics-events-with-cypress-end-to-end-testing.md)
- [How can I save Cypress screenshots + logs in GitlabCI?](how-can-i-save-cypress-screenshots-+-logs-in-gitlabci.md)
- [How can I control 3rd party exceptions with Sentry and NodeJS?](how-can-i-control-3rd-party-exceptions-with-sentry-and-nodejs.md)

## About this repository

I use spikes to structure my research work. It helps me to define what I want to find out. With practice, I've understood that documenting spikes allows me to go back to them in the future and refresh what I learned.

Hope this research is also useful for you =)
