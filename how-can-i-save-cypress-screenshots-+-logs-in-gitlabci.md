# How can I save Cypress screenshots + logs in GitlabCI?

## Context

Date: 15-11-2019
Project: https://eaglestatus.io

I've recently faced the annoying "this test runs perfectly in my machine, but fails randomly in the CI". It is very hard to debug this failure since there is only the test output information. Not enough.

The idea is:

- Find out how to save the screenshots cypress takes when a test fails.
- Find out how to store application logs when a test fails.

## Research done

### Cypress screenshots

Cypress takes a screenshot when a test fails and stores it in `cypress/screenshots` directory .

This is an example:

![alt text](./media/cypress_terminal_error.png "Error example")


![alt text](./media/cypress_screenshot_example.png "Screenshot example")


### Storing screenshots as GitlabCI artifacts

GitlabCI allows to store files related to a job execution. They call this [artifacts](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html).

In the case of a test failing it can be configured as follows in the [gitlab-ci.yml](https://docs.gitlab.com/ee/ci/yaml/):

```yml
e2e:
  stage: test
  image: tmaier/docker-compose:18.09
  services:
    - docker:dind
  script:
    #commands to prepare and run the tests with docker-compose
  artifacts:
    when: on_failure
    paths:
     - e2e/cypress/screenshots/
    expire_in: 1 week
```

Notice:

- `artifacts` defines what should be stored and when.

_In my case, I use Docker, the screenshots are taken inside the container. This means that GitlabCI will not have access to them. This can be solved by defining a volume with docker-compose._

### Storing application logs as GitlabCI artifacts

In my case I can get all the logs involved in the end-to-end tests with the following command:

```
docker-compose logs app front stub > ci.log
```

The `gitlab-ci.yml` looks as follows:

```yml
e2e:
  stage: test
  image: tmaier/docker-compose:18.09
  services:
    - docker:dind
  script:
    #commands to prepare and run the tests with docker-compose
  after_script:
    - docker-compose logs app front stub > ci.log
  artifacts:
    when: on_failure
    paths:
     - e2e/cypress/screenshots/
     - ci.log
    expire_in: 1 week
```

Notice:

- `after_script` section will run even if the tests fail (script section).
- The `ci.log` is included as another artifact.

Having this done, each time an end-to-end test fails in GitlabCI, it will generate an `artifacts.zip` containing both the screenshots and the logs.

## Conclusions

- Screenshots can be saved as artifacts when a test fails.
- Logs can be saved as artifacts when a test fails.
- Debugging can be done making use of the artifacts.zip download.
- CONS: Screenshot artifacts require e2e to use volumes in the CI.

## Sources

- https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html
- https://docs.gitlab.com/ee/ci/yaml/
- https://docs.cypress.io/guides/guides/screenshots-and-videos.html#Screenshots
- https://docs.docker.com/compose/compose-file/

## Related spikes

- [How can I test GoogleAnalytics events with Cypress end-to-end testing?](how-can-i-test-google-analytics-events-with-cypress-end-to-end-testing.md)
- [How can I control 3rd party exceptions with Sentry and NodeJS?](how-can-i-control-3rd-party-exceptions-with-sentry-and-nodejs.md)