# How can I test GoogleAnalytics events with Cypress end-to-end testing?

## Context

Date: 8-1-2020
Project: https://eaglestatus.io/

I'm using [GoogleAnalytics](https://marketingplatform.google.com/about/analytics/) in my front-end application. I want to find out if it is possible to cover the analytics events with end-to-end testing.

The idea is:

- From [Cypress](https://www.cypress.io/) interact with the application
- Register somewhere the events being tracked (localStorage is the cheapest I can think of).
- Assert against the events registered

## Research done

### Store user events in LocalStorage only in development

User events can be stored in localStorage. It relies on a setup that allows to conditionally include GoogleAnalytics `gtag`. See [How can I load Google Analytics script only in production with Webpack?](./how-can-i-load-google-analytics-script-only-in-production-with-webpack.md)

```js

const track = (category, event, data = {}) => {
  const { label, value } = data
  const gtag = window.gtag || devTag
  gtag('event', event, { 
    event_category: category,
    event_label: label || '',
    value: value || 0
  })
}

const devTag = (_text, action, data) => {
  const event = { action, ...data }
  console.log('TRACKING', event)
  
  const KEY = 'TRACKING'
  const tracks = JSON.parse(localStorage.getItem(KEY) || '[]')
  tracks.push(event)
  localStorage.setItem(KEY, JSON.stringify(tracks))
}

export default track
```

Notice:

- With my custom `track` function, in development it will make use of `devTag`. This means that events will be logged and stored in the local storage in development, but in production it will only send the data to GoogleAnalytics.


This is an example of how it is used when a user clicks a button in React:

```js
const ChooseServicesButton = ({ onClick }) => {
  const handleClick = () => {
    track('home', 'choose_services')
    onClick()
  }

  return (
    <button
      id="chooseServices"
      className="button is-primary is-rounded"
      onClick={handleClick}
    >
      Choose your services
    </button>
  )
}
```

### Assert against localStorage from Cypress


```js
const { goToHomePage,  clearAnalytics } = require('./helpers/steps')

describe('Trackeable', () => {
  afterEach(() => {
    clearAnalytics()
  })

  it('registers events of a user in the application', () => {
    goToHomePage().clickChooseYourServices()

    tracked('home', 'choose_services')
  })

  function tracked(category, action) {
    const expectedTrack = {
      action,
      event_category: category
    }
    cy.should(() => {
      const tracks = JSON.parse(localStorage.getItem('TRACKING') || '[]')
      const found = tracks.find((track) => {
        return (track.action === expectedTrack.action) &&
          (track.event_category === expectedTrack.event_category)
      })
      if (!found) {
        expect(JSON.stringify(tracks)).to.include(JSON.stringify(expectedTrack))
      }
    })
  }
})
```

Notice:

- `cy.should`, allows you to assert against localStorage.
- `cy.clearLocalStorage` clears the keys you want from localStorage:
  ```js
  const clearAnalytics = () => {
    cy.clearLocalStorage('TRACKING')
  }
  ```

## Conclusions

- It is possible to register analytics events into local storage from application. 
- It is possible to assert against local storage from Cypress.
- CONS: Setup should allow differences between development and production. 

## Sources

- https://example.cypress.io/commands/local-storage
- https://docs.cypress.io/api/commands/should.html
- https://marketingplatform.google.com/about/analytics/
- https://developers.google.com/gtagjs/reference/api
- https://www.cypress.io/


## Related spikes

- [How can I load Google Analytics script only in production with Webpack?](./how-can-i-load-google-analytics-script-only-in-production-with-webpack.md)
- [How can I save Cypress screenshots + logs in GitlabCI?](./how-can-i-save-cypress-screenshots-+-logs-in-gitlabci.md)