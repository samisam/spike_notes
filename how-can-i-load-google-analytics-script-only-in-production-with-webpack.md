# How can I load Google Analytics script only in production with Webpack?

## Context

Date: 22-11-2019
Project: https://eaglestatus.io/


In order to send user events to [GoogleAnalytics](https://marketingplatform.google.com/about/analytics/) I have to include a script they provide:

```html
<script async src="https://www.googletagmanager.com/gtag/js?id=XXX"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag() { dataLayer.push(arguments); }
  gtag('js', new Date());
  gtag('config', 'XXX');
</script>
```

I've added the script directly in the `index.html`. This causes that each time the end-to-end tests run the analytics are polluted. **The site becomes more popular when I run the tests!!!**

The purpose of this spike is to figure out how to include the `gtag `script only for production.


## Research done

### Plugins available

- HtmlPartialsWebpackPlugin: "Easy HTML partials for Webpack without a custom index. Relies on html-webpack-plugin 4+ (currently at beta)."

- HtmlWebpackPlugin: "The plugin will generate an HTML5 file for you that includes all your webpack bundles in the body using script tag."

### Include analytics scripts in production

This is how the webpack configuration file looks like for production:

```js
# webpack.prod.js

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackPartialsPlugin = require('html-webpack-partials-plugin')

module.exports = {
  mode: 'production',
  entry: './src/index.js',
  module: {
    rules: [
      /// babel, sass and other rules
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: path.resolve(__dirname, 'public/dist'),
    filename: 'js/bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/html/index.html',
      inject: false
    }),
    new HtmlWebpackPartialsPlugin({
      path: './src/html/_analytics.html',
      location: 'head',
      priority: 'high'
    }),
    /// css and other plugins
  ]
}
```

Notice:

- The `plugins` section where HtmlPartialsWebpackPlugin and HtmlWebpackPlugin are registered.
- The `_analytics.html` looks like this:

```html
<script async src="https://www.googletagmanager.com/gtag/js?id=XXX"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag() { dataLayer.push(arguments); }
  gtag('js', new Date());
  gtag('config', 'XXX');
</script>
```

- The `_index.html` contains the html to use as base:

```html
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>EagleStatus | All-in-one status page</title>
    <link rel="stylesheet" href="/reset.css" />
    <link rel="icon" type="image/png" href="/img/favicon.ico" />
    <link href="/dist/css/styles.css" rel="stylesheet" />
  </head>
  <body>
    <div id="main"></div>
    <script src="/dist/js/bundle.js"></script>
  </body>
</html>
```


### Exclude analytics from dev build

This is how the development webpack config looks like:

```js
# webpack.dev.js

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  module: {
    rules: [
      /// babel, sass and other rules
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: path.resolve(__dirname, 'public/dist'),
    filename: './js/bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/html/index.html',
      inject: false
    }),
     /// css and other plugins
  ]
}
```

Notice:

- In this case the analytics partial html is not included.
- Duplication between dev and prod can be removed with webpack-merge plugin.

### Wrapping gtag

Right now, although the script is not included, the call to `gtag` is being made from the JS code. This can be avoided by wrapping the `gtag` function:

```js

const track = (category, event, data = {}) => {
  const { label, value } = data
  const gtag = window.gtag || devTag
  gtag('event', event, {
    event_category: category,
    event_label: label || '',
    value: value || 0
  })
}

const devTag = (_text, action, data) => {
  const event = { action, ...data }
  console.log('TRACKING', event)
}

export default track
```

This way in development tracked events will appear in the console.

This is an example of how it is used when a user clicks a button in React:

```js
const ChooseServicesButton = ({ onClick }) => {
  const handleClick = () => {
    track('home', 'choose_services')
    onClick()
  }

  return (
    <button
      id="chooseServices"
      className="button is-primary is-rounded"
      onClick={handleClick}
    >
      Choose your services
    </button>
  )
}
```

### Choosing the build

This is done in the `package.json` scripts:

```js
"scripts": {
....
    "build": "webpack --config build/webpack.dev.js",
    "build-production": "webpack --config build/webpack.prod.js --mode production"
}
```

## Conclusions

- HTML can be conditionally included using webpack and the plugins `html-webpack-plugin` and `html-webpack-partials-plugin`.
- It requires to split webpack config files: production + development.
- It requires to treat html as part of the src, as if it was a JS or CSS file.

- CONS: Duplication between prod and dev config. It can be solved with webpack merge plugin.
- CONS: html-webpack-plugin is in beta for webpack 4. Its JS maddafakka!

## Sources

- https://www.freecodecamp.org/news/how-to-set-up-reliable-and-maintainable-google-analytics-in-webpack-6b68bfde29b3/
- https://webpack.js.org/plugins/html-webpack-plugin/
- https://webpack.js.org/guides/production/
- https://developers.google.com/gtagjs/reference/api


## Related spikes

- [How can I test GoogleAnalytics events with Cypress end-to-end testing?](how-can-i-test-google-analytics-events-with-cypress-end-to-end-testing.md)